/*
* ������ ��������������� ������� �������� ���-12�
*
* ��������� ����������� � ������� ����������. ���������������� �� 4-� ������
* ������������ ������� �� 1-wire
*
* MCU - MSP430I2041
* DAC - DAC161S997
* Internal freq - 16.384 MHz
* 
* ����������: ������ �.�.
* �����������: ��� "�������-��������"
* ���� ������: 06.06.2018
* ���� ����������: 22.06.2018
*/

//#include "io430.h"
#include "msp430.h"
#include "math.h"

#define CPU_FREQ        	16384000		// 16.384 ��� ������� ����������� DCO
#define U_REF           	1.158           // ������� ���������� ��� ���
#define Addr_EEPROM_conf 	0xF000          // ����� ������ ������/������ �������� �������
#define Addr_Segm1			0xF800 			// ������������� ������������ ��0-��23
#define Addr_Segm2			0xF400 			// �������� ����� �������, ��, �� �� ���. ������ �������, �� ��������� �������
#define Addr_Segm3			0xF000 			// ��������� �������
#define Addr_Segm4			0xEC00 			// ���, ����������, ����, ���������
#define Addr_Segm5			0xE800 			// ��������� �������� ������, ���.����� ������� ��� �������� ������
#define Addr_Segm6			0xE400 			// ������� ���� �������

#define Addr_cur_EEPROM_conf 0xE400       	// ����� ������ ������/������ ������� ������������ �������

//----������ ���----------------------------------------------------------------
#define SCLK_DAC        7                   // �2.7
#define SDI_DAC         6                   // �2.6
#define SDO_DAC         0                   // �2.0
#define CSB_DAC         5                   // �2.5
#define PORT_DAC        P2OUT               // P2

#define TX_PIN 			3
#define RX_PIN 			2
#define PORT_UART		P1OUT

#define TX_IN 			P1SEL0 &= ~BIT3
#define WDT_START       (WDTPW+WDTTMSEL+WDTCNTCL+WDTSSEL+WDTIS0)

//----���������������� ��������� ������� ������ ����������----------------------------------
#define Led_short_blink 45                                                                  // ������� ��������� �������� ��� ���������� ��� �������
#define Led_long_blink  150                                                                 // �������� ��������� �������� ��� ���������� ��� ������� (��� ��������� ������������ � ������� �������� delay_err_mass[])
#define off             0x00                                                                // ��������� ���������� �� ��������
#define on              0xFF                                                                // ��������� ���������� �������� (��������� ������������ � ������� ��������� led_state_mass)
//----����/������ ������--------------------------------------------------------------------
#define ALL_OK			0 																	// ��� ��
#define PRES_OVER_HI	1 																	// ���������� �������� ���� 1.1 ��
#define PRES_OVER_LO	2 																	// ���������� �������� ���� 0.01 ��, ��� ��� ���������� ���������� ������ 1.01 ��(-)
#define SET_ZERO_ERR	3 																	// ��� ��������� ���� �� ���������� ��������� �������� �������� ���� ������� �� ���������� �������
#define ADC_ERR 		4 																	// ������ ���
#define EEPROM_ERR 		5 																	// ������ ����������� ����� ������
#define SENS_ERR		6																	// ������ �������

//----������ ��������� ������������ (Namur)-------------------------------------------------
#define CRIT_ERR_LOW			3.6 														// ������������� ������� ������ ��� ������ ���������� ������� (����������� ������)
#define CURRENT_OUT_ALARM_LOW	3.8 														// ������������� ������� ������ - ���� ����������� ��������
#define CURRENT_OUT_ALARM_HIGH	20.5 														// ������������� ������� ������ - ���� ����������� ��������
#define CRIT_ERR_HIGH			22.5						                                // ������������� ������� ������ ��� ������ ���������� ������� (����������� ������)


//----������������--------------------------------------------------------------------------
void I_ADC_write (unsigned char byteword);                                                  // ������ ������ � ��� ���������� ����
unsigned long I_ADC_read (void);                                                            // ������ ������ �� ��� ���������� ����
void DAC_send_data (unsigned char DAC_Addr, unsigned long DAC_data);                        // �/� �������� 24 ��� ������ �� ����������� � ���
void OUT_current (void);                                                                    // �/� ������������� ��� � ������� �����
void critical_error (void);                                                                 // �/� ������ ������ ������ �� ��������� ��� ��������� ��������� � ��������� ���������� ������� � ������� �����
void Search_problem (void);                                                                 // �/� ����������� ������ � �������

void USART_Init (void);                                                                     // ������������� USART
void Port_Mapping(void);                                                                    // �������������� ������� ����������� (����� ACLK �� �1.0)
void Led_error_ind (void);                                                                  // ����� ������ ��� ������ ����������
void Set_error (unsigned char err);                                                         // ���������� ��� (���) ������, ���������������� ���������� � ��������� ��������� ������ �����������

void READ_4BYTE (unsigned char *ptr2, unsigned int addr);                                   // ������ 4-� ���� �� EEPROM
unsigned char EEPROM_read(unsigned int adress);                                             // ������ 1-�� ����� �� EEPROM
void read_conf (void);                                                                      // ������ �������� ������� �� EEPROM
void CALL_read (void);                                                                      // ������ ������������� ������������� �� EEPROM
void read_store_conf (void);                                                                // ������ �������� ����������� ������������ ������������� ������� �� EEPROM
void write_current_conf(void);																// �/� ������ � EEPROM ������� ������������ �������
void EEPROM_write_pressure_cal (void);                                                      // �/� ������ � EEPROM ���������� ������� �� �������� * ������� 1 * 0xFDFF-0xFC00
void write_EEPROM_ASSEMBLY_NUMBER (unsigned char *prt);                                     // �/� ������ Final Assembly Number * ������� 2 * 0xFBFF-0xFA00
void write_data_PD (unsigned char *prt);                                                    // �/� ������ � EEPROM ������ �� * ������� 3 * 0xF9FF-0xF800
void write_EEPROM_TEG (unsigned char *prt);                                                 // �/� ������ � EEPROM ����,����,��������� * ������� 4 * 0xF7FF-0xF600
void write_EEPROM_MESSAGE (unsigned char *prt);                                             // �/� ������ � EEPROM ��������� * ������� 5 * 0xF5FF-0xF400
void write_EEPROM_SERIAL_PD (unsigned char *prt);                                           // �/� ������ � EEPROM ��������� ������ �� * ������� 6 * 0xF3FF-0xF200
void write_EEPROM_ZAVOD_NUMBER_DD (unsigned char *prt);                                     // �/� ������ � EEPROM ���������� ������ ��* ������� 7 * 0xF1FF-0xF000
void EEPROM_write_call (void);                                                              // �/� ������ � EEPROM ������������� ������������� * ������� C * 0x18FF-0x1880
void EEPROM_write_conf (void);                                                              // �/� ������ � EEPROM ������������ ������� * ������� D * 0x187F-0x1800
void write_EEPROM_diapazon_change (void);                                                   // �/� ������ � ������ ���������� �������������
void Scan_dip_switch (void);                                                                // ������������ ���-��������������

float okruglenie (float abc, unsigned char num);                                            // �/� ���������� ����� ���� float
float perevod_v_davlenie (float znach);                                                     // �/� �������� � ������� ��. ���������
float perevod_v_uslovnie_edinici (float znach, char edinica);                               // �/� �������� � ������� ��. ���������
char proverka_delta_P (float delt_P);                                                       // �/� ��� �������� ������������ ��������� ����

void CRC16 (unsigned char *nach, unsigned char dlina);                                      // �/� ������� CRC16 �������� �������� 0xA001 

void Zero_Button_Event(void);                                                               // �/� ��������� ������� � ������
void Zero_Magnit_Event (void);                                                              // �/� ��������� � �������

//----����������----------------------------------------------------------------
// ��� � �����
unsigned int  b_current = 0x2AB4;                                               // ������������ ��� ���������� ���� ��� ���������
float k_current = 0.00036610;                                                   // �������� ��������� ���� �������� �� ���������

float loop_current;                                                             // ��� � �����

unsigned char flag_i_out_auto = 1;
unsigned long b_ADC_mosta, r_ADC_mosta;                                                     // � ��� �������� �������� ����� ��� ������ ��� �������� �� �� HART ���������

// �������� ��������
int timA0count = 0;                                                                         // ���������� ������� ����������
int timD0count = 0;                                                                         // ���������� ������� ����������

unsigned char *Flash_ptr;                                                                   // ��������� �� ������ ������ � ������� � EEPROM
unsigned char *CALL_ptr;                                                                    // ��������� �� ������ ������� � �������������� ��������������
float ca [24];                                                                              // ������ � �������������� ����������
unsigned char edinica_izmer = 0x0C;                                                         // ������� ���������

//float temperatura=0, u=0, p=0, i_out=0, procent_diappazona = 0, f;

float R_mosta;                                                                              // ���������� ��� ������������� �����
float temperatura=0, u=0, p=0, i_out=0, procent_diappazona = 0, f; 
float davlenie;
unsigned long bb, r;                                                                        // ������������� �����
float a0,a1,a2,a3,a4,a5;                                                                    // ������������ ��� ������������� ���������� �� �����������
float temp_u;                                                                               // ���������� ���������� � ���������� � ��
unsigned char flag_ADC, flag_ADC_IDLE=0;                                                    // ���� ��������� ���������� �� ������ ����������, ���� ��������� ���������

unsigned long Skolzyashie_okno[255];                                                        // 128
unsigned long *Rx_skolz_okno, *Tx_skolz_okno;

unsigned long Skolzyashie_okno_R [8];
unsigned long *Rx_skolz_okno_R, *Tx_skolz_okno_R;    

unsigned char count_time;                                                                   // ������� ������� �0 ��� ��������� ���������� ������� �����������
unsigned char Term_time=0, Term_flag=1;                                                     // ������� ������� ������� �����������, ���� ������ ������� � ��������� �����������                                                                     


unsigned char tok_signal_inv=0;                                                             // �������� ������� ������ 4-20 �� ��� 20-4 ��
unsigned char koren = 0x00;                                                                 // ���������������� ����������� ���������    
unsigned char HART_disable = 0;                                                             // ���������� ������������ �� HART
unsigned char magnit_key_enable=1;                                                          // ���������� ������ ��������� ������
unsigned char tmp_current_alarm, current_alarm = 0;                                         // ��������� �������� ���� ��� ��������� ������ 
float diapazon_v = 1.000;                                                                   // ������� �����������
float diapazon_n = 0.000;                                                                   // ������ �����������
float tmp_diapazon_v, tmp_diapazon_n;                                                       // ������������ ��� ��������� ����� �������� �� HART
float min_diapazon_PD, diapazon_n_PD;                                                       // ������������ ��� ������������� �������� ���������
float delta_P = 0;                                                                          // �������� �������� ��� ���������� 0

unsigned char N_integr;                                                                     // 32 ��������� ��� ��������������!�� �����!
unsigned char N_integr_R = 8;
unsigned char N_integr_tmp=1;
unsigned char flag_R = 1;

float mastb;                                                                                // ���������� ����������� ��� �������� �������� � ������ ���������

float tok_low_input, tok_hi_input;                                                          // ��� ������� ������������� ���������� ��� ����� ����
float znach_vpi, znach_npi;                                                                 // ������������ ��� ���������� �� ��������

float k_pressure = 1, b_pressure = 0;                                                       // ��������� �������� ��������� �������������� �� ��������
unsigned char n;
unsigned char i;

unsigned char crc_H, crc_L;                                                                 // �������� ����������� �����

float pr [16];                                                                              // ������ �������� ������������ ������� � dip-��������������
unsigned char ed_izmer_eeprom = 0x0C;                                                       // ��� ������� ��������� ��� �������� ������������ ������� 0�0� - ���
unsigned char dip_switch_value = 0x00;                                                      // ���������� - ��������� ��� �������������� ���������� 00000111 ��� �������� 0�07 (��� ������������� OFF) ������ ��������� ������� ������������� ���������� �� �����


// ���������� ��� ��������� ������                                                          
/* 
* 10 ����� ������, �������� ��� ������ ���� ��������:                                 
* 1-� ������ �������� ��������� ��������� - ��������� ��������/�� ��������            
* 2-� ������ �������� ��������� ���������� � ���������� ������ �������                
* ������ ����������� ��������� ������� �� 1-�� �������, ���������� ����,               
* �/� ��������� ������ ������ ��������� ���������� �������� ������� 2                 
* ��������(������ �������) += 1 � ��� �������.                                        
*/
unsigned char type_error = 0;                                                               // ��� (���-�����) ������
int delay_from_err_mass = 0;                                                                // ����������, � ������� ������������ �������� �������� ��� ������� �� �������, � ������� ������� ��������� ��������/�� ��������
// ������ � ����������� ������� � ������� ������� ��������� ������ ��������/�� ��������
int delay_err_mass[26] = {2*Led_long_blink, Led_short_blink, Led_short_blink, Led_short_blink, Led_short_blink, Led_short_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink, \
	Led_long_blink, Led_long_blink};
// ������ � ����������� ���������� ��������/ �� ��������, ����� 10 ������, + 3 ������� ��� �����������
unsigned char led_state_mass[26] = {off, on, off, on, off, on,\
off, on,\
off, on,\
off, on,\
off, on,\
off, on,\
off, on,\
off, on,\
off, on,\
off, on,\
off, on,};
unsigned char tim_err_flag = 0;                                                             // ���� - ������ ������� �������� ��������� �������� ��� ����������
unsigned char err_itter = 0;                                                                // ��������� �� ������� ��������� ���������� �/� ��������� ������ ( ������ ������ �� ������� �������� � ��������� ���������� )
unsigned char flag_error = 0;                                                               // ���� ������� ������ � �������
unsigned char count_error_ind = 0;															// ������� ���������� ��������� ����� � ��� �� ������

// ���������� ����������� �� ������ �������
int tmp = 0, er_tmp = 0;                                                                    // ������� ������� �0 �� �������� ��������� ������� ��� ������ +1; ���������� ������� ������ ������� ��� ������

// ����������, ������� ���������� ��� ��������� ������� � ������ ���� �������
unsigned char zero_but_process_flag = 0;                                                    // ���� - ������� ������� ��������� ������� � ������
unsigned char zero_mag_process_flag = 0;                                                    // ���� - ������� ������� ��������� ������� � ��������� ������
unsigned char zero_process_count = 0;                                                       // ������� ��� ������� ��������� ���������� ��� ��������� �������
unsigned char zero_but_process_itteration = 0;                                              // ��������� �������� ��������� ������� � ������
unsigned char zero_mag_process_itteration = 0;                                              // ��������� �������� ��������� ������� � ��������� ������
unsigned char zero_process_disable_flag = 0;                                                // ���� ����������� ���������
unsigned char zero_but_mag_prioritet_flag = 0;                                              // ���� ���������� = 2 � ������� ������ ��������� ���� � �������, = 1 ��������� ���� � ������

unsigned char modelPD [] = {0,'2','1','5','0','M'};											// ��� ������� �������� 0x00 � ��, 0x01 � ��, 0x02 � ���, 0x03 � ��, 0x04 ���; ������ �������

unsigned long u24_adc_H = 0;																// ��������� ���������� � ��� (������� ����)
unsigned long u24_adc_L = 0;																// ��������� ���������� � ��� (������� ����)
volatile unsigned long u24_adc = 0;															// ��������� ���������� � ���
volatile unsigned int temp_adc = 0;             											// �������� ����������� ������ � ���
unsigned char flag_MEM = 0;                 												// ���� ������ �������� ����� ���


#include "low_level_init.c"
#include "USART_1WIRE.c"                                                        // ������ USART
void main( void )
{
	// Stop watchdog timer to prevent time out reset
	WDTCTL = WDTPW + WDTHOLD;

	P1DIR |= BIT1 + BIT0 + BIT4 + BIT5 + BIT6 + BIT7;
	P1OUT &= ~(BIT1 + BIT0 + BIT4 + BIT5 + BIT6 + BIT7);

	P2DIR |= BIT1 + BIT2 + BIT3 + BIT4;
	P2OUT &= ~(BIT1 + BIT2 + BIT3 + BIT4);

	//----��������� ������ �����/������-----------------------------------------
	P2DIR |= BIT7 + BIT6 + BIT5;                                      			// ���� �� �����
	P2OUT &= ~(BIT7 + BIT6 + BIT5);                                   			// ���. 0 �� ������� �����
	P2OUT |= BIT0;																// SDO_DAC pull-up

//	P1DIR |= BIT0;	// MCLK
//	P1OUT &= ~BIT0;																// ��� ��� �������
	// P1SEL1 |=BIT0;	//MCLK

	__low_level_init();

	// Configure SMCLK = MCLK = 4MHz
    CSCTL1 |=  DIVM__4 | DIVS__4;     // MCLK = DCO/4, SMCLK = DCO/4 = 4,096V ���
//	FCTL2 = FWKEY | FSSEL_1 | FN1 | FN3 | FN5;  								// MCLK/42 for Flash Timing Generator
    FCTL2 = FWKEY | FSSEL_1 | FN1 | FN3;  										// MCLK/10 for Flash Timing Generator

	/*//----��������� ������� �0------------------------------------------------
	TA0CCTL0 = CCIE;                                                            // ���������� ���������� �� ���������� TA0CCR0
	TA0CCR0 = 2000;                                                             // ��������, �� �������� ������� ������ ������� (3686400 / 4 / 2000 = 460,8 ���������� � �������)
	TA0CTL = TASSEL_2 + MC_1 + ID_0;                                            // ������������ �� SMCLK, ����� ����� - �����, ������������ - 1
	*/
	//----��������� ������� A1--------------------------------------------------
	TA1CCTL0 = CCIE;                                                            // ���������� ���������� �� ���������� TD0CCR0
	TA1CCR0 = 288;                                                              // ��������, �� �������� ������� ������ ������� (32000  / 288 = 111 ���������� � ���)
//    TA1CCR0 = 320;                                                              // ��������, �� �������� ������� ������ ������� (32000  / 320 = 100 ���������� � ���)
	TA1CTL = TASSEL_1 + MC_1 + TACLR;                                           // ������������ �� ACLK, ����� ����� - �����, ������������ - 1




	//----��������� ���---------------------------------------------------------------------
	PORT_DAC |= (1<<CSB_DAC);                                                   // 1 �� ������ CSB
	// ��������� ��� - ���������� � �������� ERR_CONFIG ������ �� �������� SPI � ������ ������� �����
	DAC_send_data(0x05, 0x1403);

	USART_Init();

	WDTCTL = WDT_ARST_1000;                                                     // ������ ����������� ������� ������ 1000 ��      

	//----��������� ���---------------------------------------------------------
//	SD24CTL &= ~(1<<SD24REFS);                         							// External ref
	SD24CTL |=  SD24REFS;                         								// Internal ref
	SD24CCTL0 |= SD24GRP;                               						// ������ � ������� 1
	SD24CCTL1 |= SD24DF | SD24IE;                       						// ���������� ����������
  	SD24INCTL1 |= SD24INCH_6;                           						// ����� ������� ����������� 

    

	//----������ �������� ������� �� EEPROM-------------------------------------------------
	CALL_ptr = (unsigned char *) Addr_EEPROM_conf;
	CRC16 (CALL_ptr, 126);                                                                  // ����������� �������� CRC ��� ����� ������ � ����������� �������
	CALL_ptr = (unsigned char *) Addr_EEPROM_conf + 126;

	if (crc_H == *CALL_ptr)                                                                 // ��� ��������� ������ �������� CRC ������ � ����������� �������
	{
		CALL_ptr += 1;
		if (crc_L == *CALL_ptr)
		{
			read_conf ();                                                                   // ���� ����������� ����� ��������� - ������ �������� ������� �� EEPROM
		}
		else                                                                                // ���� ����������� ����� EEPROM �� ��������� - �� ������� �� ��������� ��������� �� ������
		{                                                                                   // Err-6
			flag_error = 1;
			type_error = EEPROM_ERR;
			Set_error(EEPROM_ERR);   
		}
	}
	else                                                                                    // ���������� ���-��� ������ ����� 6
	{
		flag_error = 1;
		type_error = EEPROM_ERR;
		Set_error(EEPROM_ERR); 
	}

	//----������� ������������ ������������� �������������� �� ��������---------------------
	CALL_ptr = (unsigned char *) 0xF413 + 8;                                                // �������� ���������� ������ ���������� �� �������
	if (*CALL_ptr != 0x00)                                                                  // ���� ��� ������ ��������� ����� �������� - ���������� ������������ �������� �� ��������� (k=1, b=0)
	{
		EEPROM_write_pressure_cal ();                                                       //
	}
	else                                                                                    // ���� ������ ��� ���� ���������, ��������� �������� ������������� ������������� �������������� �� ��������
	{
		CALL_ptr = (unsigned char *) & k_pressure;
		READ_4BYTE (CALL_ptr, 0xF413);                                                      //

		CALL_ptr = (unsigned char *) & b_pressure;
		READ_4BYTE (CALL_ptr, 0xF417);                                                      //
	}
	//--------------------------------------------------------------------------------------

	CALL_read ();                                                                           // ������ ������������� �������������

	CALL_ptr = (unsigned char *) & mastb;
	READ_4BYTE (CALL_ptr, 0xF401);                                                          //

	CALL_ptr = (unsigned char *) 0xF400;
	if (*CALL_ptr == 0x0C) mastb = mastb * 1;                                               // ������ ����������� ������������ (�������� ������� ��������� ��) ��� ������� ��������
	else mastb = mastb * 1000;                                                              // ���� �������� ������� � ���, ����� ��������� ��������� �� 1000

	CALL_ptr = (unsigned char *) & znach_vpi;
	READ_4BYTE (CALL_ptr, 0xF401);                                                          // ��������� �������� �������� ��������� ��������� ��������

	CALL_ptr = (unsigned char *) & znach_npi;
	READ_4BYTE (CALL_ptr, 0xF405);                                                          // ��������� �������� �������� ��������� ��������� ��������

	diapazon_n_PD = znach_npi / znach_vpi;
	min_diapazon_PD = (1.0 - diapazon_n_PD)/25.5;

	N_integr = (unsigned char) (damping_value / 0.0786) + 1;                                // ������������ �������� ���-�� �������� ��� ��������������

	if (DEV_ID != 0x00)                                                                     // ���� ����� ���������� �� ������� - ������ ������� ����� ����������
	{
		flag_i_out_auto = 0;                                                                // ������ � ����� �������������� ����
		loop_current = 4.000;
		OUT_current ();                                                                     // ������������� �������� ��������� ���� ������ 4 ��
	}

	_EINT();	                                                                			// ���������� ���������� ����������
	Tx_skolz_okno = Rx_skolz_okno = (unsigned long *)& Skolzyashie_okno;                    // ��������� ����������� ���� - �� ������ ������ (���������� ���������)
	SD24CCTL1 |= SD24SC;                                									//��������� ���� ������ ��������������

	while(1)
	{
		WDTCTL = WDTPW + WDTCNTCL;                                              			// ����� ����������� �������	
		if (CDR_END_HART == 1) parser_USART();                                  			// ���� ���������� ���� ������ ������ �����, ���� ��������� �������� �������.

	//  Sensor's temperature coefficient is 2.158mV/K (from datasheet)
	//  Sensor's offset voltage ranges from -100mv to 100mV (assume 0)
	//  Vsensor = 1.32mV * DegK + Vsensor_offset (assuming 0mv for offset)
	//  Vsensor = (SD24MEM0)/32767 * Vref(mV)
	//  DegK = (SD24MEM0 * 1200)/32767/2.158 = (SD24MEM0 * 1200)/70711
	//  DegC =  DegK - 273
		temperatura = ((((float)temp_adc * 1158)/70711) - 273);                    			// ������ ����������� � �������� �������

		i = N_integr_tmp;
		bb = 0;

		while (i)
		{
			bb = bb + *Rx_skolz_okno;                                       // ��������� �������
			Rx_skolz_okno += 1;                                             //
			i = i - 1;
		}
		bb = bb / N_integr_tmp;                                             // ����� ��������� �� ���-�� �������
		Rx_skolz_okno = (unsigned long *)& Skolzyashie_okno;                // ��������� ������ ����������� ���� - �� ������ ������

		if (N_integr_tmp < N_integr) N_integr_tmp += 1;                     // ����������� �� 1 ���-�� ������� �� ������ ���� �� ��������� �������������� ��������

		b_ADC_mosta = bb;                                                   // ��������� ��� ��� ����������� �������� ��� �������� �� HART ���������

		// ���������� ���������� �� ������ �������
		if (bb >= 0x800000)
		{
			bb = bb - 0x800000;                                             //
			u = 1000*((U_REF/0x800000)*bb);                                 // ���������� ���������� � ��
		}
		else
		{
			bb = 0x800000 - bb;
			u = 1000*(-(U_REF/0x800000)*bb);                                // ���������� ���������� � ��
		}

		f = temperatura * temperatura;                                              // ������������� ������������ ��� ������ ������������� ���������� ��� ����� �����������
		R_mosta = temperatura;

		a0 = ca[0]  + ca[1]  * temperatura + ca[2]  * f + ca[3]  * f * temperatura;
		a1 = ca[4]  + ca[5]  * temperatura + ca[6]  * f + ca[7]  * f * temperatura;
		a2 = ca[8]  + ca[9]  * temperatura + ca[10] * f + ca[11] * f * temperatura;
		a3 = ca[12] + ca[13] * temperatura + ca[14] * f + ca[15] * f * temperatura;
		a4 = ca[16] + ca[17] * temperatura + ca[18] * f + ca[19] * f * temperatura;
		a5 = ca[20] + ca[21] * temperatura + ca[22] * f + ca[23] * f * temperatura;

		// ���������� ��������
		temp_u = u;
		p = a0 + a1 * u;
		temp_u = temp_u * u;
		p = p + a2 * temp_u;
		temp_u = temp_u * u;
		p = p + a3 * temp_u;
		temp_u = temp_u * u;
		p = p + a4 * temp_u;
		temp_u = temp_u * u;
		p = p + a5 * temp_u;
		p = p * k_pressure + b_pressure - delta_P;                          // ������������� �� �������� ��������� ��� ���������� + ���� �������� ���� �� ���������� ���������

		davlenie = perevod_v_davlenie (p);

		procent_diappazona = (p - diapazon_n)/((diapazon_v - diapazon_n) / 100);  // ������� ������� ���������

		if (koren)
		{
			if (p >= 0.8 * (diapazon_v / 100))                              // 0,8% �� ������ �������� ������ ��������, � ��� ��� ���� - ������ ���������������
			{
				i_out = 4 + 16 * sqrt (p / diapazon_v);                     // ���������������� �����������
			}
			else
			{
				if (p <= 0.6 * (diapazon_v /100))
				{
					i_out = 4 + p * (16.0 / diapazon_v);                    // �� 0 �� 0,6% �� ��������� � ��� ������������� �������� ������������ �� ������ ��������� ������
				}
				else
				{
					i_out = 4.096 + ((p / diapazon_v) - 0.006) * 667.5;     // ��� �������� �� 0,6 �� 0,8% � ��� ������������� - ������ �������� ����������� � ������ ��������.
				}
			}
		} 
		else
		{
			i_out = 4 + (p - diapazon_n) * (16.0 / (diapazon_v - diapazon_n)); // ������� �������� ��������� ���� ��� �������� �����������
		}

		if (tok_signal_inv)
		{
			i_out = 24 - i_out;                                             // ����������� ������� ������ ���� ���������� ��� 20-4
		}

		if (i_out < CURRENT_OUT_ALARM_LOW)  i_out = CURRENT_OUT_ALARM_LOW;	// ��������� �������� ��������� ���� �� ���������� ���� � ��� �������� �������� NAMUR NE43
		if (i_out > CURRENT_OUT_ALARM_HIGH) i_out = CURRENT_OUT_ALARM_HIGH;

		if (flag_i_out_auto)                                                // ���� ��������� � ������ �� �������������� ����
		{
			loop_current = i_out;                                           //
			OUT_current ();                                                 // ������ �� ������ ��������� �������� ����
		}


/*
		if (!(P1IN & (1<<DOUT_ADC)))                                                        // ���� ��� �������� ��������������
		{
			I_ADC_write (0x58);                                                             // ��������� �������� - ������ 24-� ���������� �������� ������

			if (flag_ADC_IDLE)                                                              // ���� ���������� ���� ��������� ��������� - �� ���������� �������� ������ ������������.
			{
				I_ADC_read();                                                               // ��� ���������� ��� ������������ �������� ������ ��� ������������ ������� ���������
				flag_ADC_IDLE = 0;                                                          //
			}
			else
			{
				if (flag_ADC == 0x01)                                                       // ���� ���������� ���� ��������� ����������� ����������
				{
					*Tx_skolz_okno_R = I_ADC_read();                                        // ������ ��������
					Tx_skolz_okno_R += 1;
					if (Tx_skolz_okno_R > (Skolzyashie_okno_R + N_integr_R -1))             // ���� �������� � ���������� ���� N_integr ��������
					{
						Tx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R;            // ��������� ������ ����������� ���� - �� ������ ������
						flag_R = 0;
					}

					i = N_integr_R;
					r = 0;

					if (flag_R == 0)
					{
						while (i)
						{
							r = r + *Rx_skolz_okno_R;                                       // ��������� �������
							Rx_skolz_okno_R += 1;                                           //
							i = i - 1;
						}
						r = r / N_integr_R;                                                 // ����� ��������� �� ���-�� �������
						Rx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R;            // ��������� ������ ����������� ���� - �� ������ ������

						r_ADC_mosta = r;                                                    // ��������� ��� ��� ��� �������� �� HART ���������
						R_mosta = ((7500.0/0x7FFFFF) * (r-0x800000) * 3.8666)/4;

						R_mosta = okruglenie (R_mosta, 1);

						flag_ADC = 0x02;                                                    // ����������� �� ��������� ����������� � ������� ����������� ������� ����������� ���
						I_ADC_write (0x10);                                                 // ����. ������ � Configuration register (b'00010000')
						I_ADC_write (0x00);                                                 // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����
																							// ������������� ����. �������� �=1,
						I_ADC_write (0x96);                                                 // ���������� �������� �������� ���������� (b'10010110')
																							// �������������� �����, ����������� ��� �� ��������� ����������� � ������� ����������� �������
						flag_ADC_IDLE = 1;                                                  // ��������� ��������� ��� - ����������.

						f = R_mosta * R_mosta;                                              // ������������� ������������ ��� ������ ������������� ���������� ��� ����� �����������

						a0 = ca[0]  + ca[1]  * R_mosta + ca[2]  * f + ca[3]  * f * R_mosta;
						a1 = ca[4]  + ca[5]  * R_mosta + ca[6]  * f + ca[7]  * f * R_mosta;
						a2 = ca[8]  + ca[9]  * R_mosta + ca[10] * f + ca[11] * f * R_mosta;
						a3 = ca[12] + ca[13] * R_mosta + ca[14] * f + ca[15] * f * R_mosta;
						a4 = ca[16] + ca[17] * R_mosta + ca[18] * f + ca[19] * f * R_mosta;
						a5 = ca[20] + ca[21] * R_mosta + ca[22] * f + ca[23] * f * R_mosta;
					}
				}
				else
				{
					if (flag_ADC == 0x02)
					{
						r = I_ADC_read();                                                   // ������ ��������
						temperatura = (((1.17/0x800000) * (r-0x800000)) / 0.00081) - 273;   // �������� � �������� �������
						temperatura = okruglenie (temperatura, 1);
						flag_ADC = 0x00;
						I_ADC_write (0x10);                                                 // ����. ������ � Configuration register (b'00010000')
						//I_ADC_write (0x04);                                                 // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����   // ������������� ����. �������� �=16,
						I_ADC_write (0x00);                                                       // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����            
																							// ������������� ����. �������� �=1,
						I_ADC_write (0x10);                                                 // �������� �������� �������� ���������� (b'00010000')
																							// �������������� �����, ����������� ��� �� ��������� ���������� AIN1(+) � AIN1(�)
						flag_ADC_IDLE = 1;                                                  // ��������� ��������� ��� - ����������.
					}
					else
					{
						*Tx_skolz_okno = I_ADC_read();                                      // ������ ��������
						Tx_skolz_okno += 1;
						if (Tx_skolz_okno > (Skolzyashie_okno + N_integr -1))               // ���� �������� � ���������� ���� N_integr ��������
						{
							Tx_skolz_okno = (unsigned long *)& Skolzyashie_okno;            // ��������� ������ ����������� ���� - �� ������ ������
						}

						i = N_integr_tmp;
						bb = 0;

						while (i)
						{
							bb = bb + *Rx_skolz_okno;                                       // ��������� �������
							Rx_skolz_okno += 1;                                             //
							i = i - 1;
						}
						bb = bb / N_integr_tmp;                                             // ����� ��������� �� ���-�� �������
						Rx_skolz_okno = (unsigned long *)& Skolzyashie_okno;                // ��������� ������ ����������� ���� - �� ������ ������

						if (N_integr_tmp < N_integr) N_integr_tmp += 1;                     // ����������� �� 1 ���-�� ������� �� ������ ���� �� ��������� �������������� ��������

						b_ADC_mosta = bb;                                                   // ��������� ��� ��� ����������� �������� ��� �������� �� HART ���������

						// ���������� ���������� �� ������ �������
						if (bb >= 0x800000)
						{
							bb = bb - 0x800000;                                             //
							u = 1000*((U_REF/0x800000)*bb);                                 // ���������� ���������� � ��
						}
						else
						{
							bb = 0x800000 - bb;
							u = 1000*(-(U_REF/0x800000)*bb);                                // ���������� ���������� � ��
						}

						// ���������� ��������
						temp_u = u;
						p = a0 + a1 * u;
						temp_u = temp_u * u;
						p = p + a2 * temp_u;
						temp_u = temp_u * u;
						p = p + a3 * temp_u;
						temp_u = temp_u * u;
						p = p + a4 * temp_u;
						temp_u = temp_u * u;
						p = p + a5 * temp_u;
						p = p * k_pressure + b_pressure - delta_P;                          // ������������� �� �������� ��������� ��� ���������� + ���� �������� ���� �� ���������� ���������

						davlenie = perevod_v_davlenie (p);

						procent_diappazona = (p - diapazon_n)/((diapazon_v - diapazon_n) / 100);  // ������� ������� ���������

						if (koren)
						{
							if (p >= 0.8 * (diapazon_v / 100))                              // 0,8% �� ������ �������� ������ ��������, � ��� ��� ���� - ������ ���������������
							{
								i_out = 4 + 16 * sqrt (p / diapazon_v);                     // ���������������� �����������
							}
							else
							{
								if (p <= 0.6 * (diapazon_v /100))
								{
									i_out = 4 + p * (16.0 / diapazon_v);                    // �� 0 �� 0,6% �� ��������� � ��� ������������� �������� ������������ �� ������ ��������� ������
								}
								else
								{
									i_out = 4.096 + ((p / diapazon_v) - 0.006) * 667.5;     // ��� �������� �� 0,6 �� 0,8% � ��� ������������� - ������ �������� ����������� � ������ ��������.
								}
							}
						} 
						else
						{
							i_out = 4 + (p - diapazon_n) * (16.0 / (diapazon_v - diapazon_n)); // ������� �������� ��������� ���� ��� �������� �����������
						}

						if (tok_signal_inv)
						{
							i_out = 24 - i_out;                                             // ����������� ������� ������ ���� ���������� ��� 20-4
						}

						if (i_out < CURRENT_OUT_ALARM_LOW)  i_out = CURRENT_OUT_ALARM_LOW;	// ��������� �������� ��������� ���� �� ���������� ���� � ��� �������� �������� NAMUR NE43
						if (i_out > CURRENT_OUT_ALARM_HIGH) i_out = CURRENT_OUT_ALARM_HIGH;

						if (flag_i_out_auto)                                                // ���� ��������� � ������ �� �������������� ����
						{
							loop_current = i_out;                                           //
							OUT_current ();                                                 // ������ �� ������ ��������� �������� ����
						}

						if (Term_flag == 1)                                                 //���� ������ ����� �������� ����������� ����������
						{
							flag_ADC = 0x01;
							I_ADC_write (0x10);                                             // ����. ������ � Configuration register (b'00010000')
						//  I_ADC_write (0x02);                                             // Bias Voltage Generator Disable, burnout currents are disabled. ���������� ����� // ������������� ����. �������� �=4,
							I_ADC_write (0x00);                                             // Bias Voltage Generator Disable, burnout currents are disabled. ���������� �����            
																							// ������������� ����. �������� �=1,
							I_ADC_write (0x11);                                             // �������� �������� �������� ���������� (b'00010000')
																							// �������������� �����, ����������� ��� �� ��������� ���������� AIN2(+) � AIN2(�)
							Term_flag = 0;
							flag_ADC_IDLE = 1;                                              // ��������� ��������� ��� - ����������.
						}
					}
				}
			}
		}

*/


//        OUT_current(); 
		Search_problem();                                                                   // ������ ������������ ������ ������ � �������������� �������
	}

}

/*
//==========================================================================================
//  �/� ��������� ���������� �� ������� A0 (��������� ����������)
//==========================================================================================
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0 (void)
{
	count_time +=1;                                                                         // +1 � �������� �������
	timA0count += 1;                                                                        // ������� ��� �/� ��������� ������ �����������
	if(timA0count > delay_from_err_mass)                                                    // ���� ������ �������� ����������� �������� � ��� ���� �� ������� ������� ��������� � ������, ������ �� ������� ��������
		tim_err_flag = 0xFF;                                                                // ���������� ���� - ������ ����������� �������� ������� - ���� ������ ��������� ����������

	if (count_time == 50)                                                                   // ���� ��������� ������ 100 ��.
	{
		count_time = 0;
		Term_time += 1;
		if (Term_time == 10)                                                                // ���� ������ 1 ���. - ���������� ������ ���������� ���������� � ������� �����������
		{
			Term_time = 0;                                                                  // ���������� ������� �������
			Term_flag = 1;                                                                  // ������������� ���� ������ ������� � �������� �����������
		}

		// ��������� � ������ ��� �������
		if((zero_but_process_flag == 1) || (zero_mag_process_flag == 1))                    //���� ������� ������� ��������� ������� � ������ ��� �������
		{
			zero_process_count +=1;
			if(zero_process_count > 250) {zero_but_process_flag =0; zero_mag_process_flag = 0; zero_process_count = 0;} // ���� ��������� ������� �����, �� ����� ������ ��� � �� ������� � ������������� ������� ���������
			if((zero_but_process_itteration == 1) || (zero_but_process_itteration == 2) || ((zero_mag_process_itteration == 1) && (flag_error == 0))) PJOUT ^= (1<<6); // ���� ��������� �� ������ ��� ������ �������� ��������� � ������, ���� 0 �� ���������� �� ������ �����������
		}

	}

	//  TD0R = 0x0000;  //���� ���������� �������� ������ D0
}
*/

//==========================================================================================
//  �/� ��������� ���������� �� ������� A1 (��������� ��� HART-���������)
//==========================================================================================
#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer1_A0 (void)
{   
	count_time_HART += 1;
	if (count_time_HART == time_8_HART) { CDR_END_HART = 1;}
	if (count_time_HART == 255) count_time_HART = 254;
}


//==========================================================================================
//  �/� ��������� ���������� �� ���
//==========================================================================================
#pragma vector=SD24_VECTOR
__interrupt void SD24_ISR(void)
{
    switch (__even_in_range(SD24IV,SD24IV_SD24MEM3)) {
        case SD24IV_NONE: break;
        case SD24IV_SD24OVIFG: break;
        case SD24IV_SD24MEM0:
        {
        //	results[0] = SD24MEM0;       // Save CH0 results (clears IFG)
        //	break;
        }     
        case SD24IV_SD24MEM1:
        {
        	if(flag_MEM == 0)
			{    
				u24_adc_H = SD24MEM0;              
				u24_adc_H = u24_adc_H << 8;                                    	// ����� �� 8 ��� ����� �������� ������� 16 ���
				u24_adc_H &= 0x00FF0000;                                    	// ��������� ������� 16 ��� 
				SD24CCTL0 |= SD24LSBACC;                                 		// ��������� ���� ������ ������� ��� ��� 0
				flag_MEM = 1;                                            		// ��������� �����, ����� � ��������� ���������� ����� ������� ��� � ���  
			}
			else
			{
				u24_adc_L = SD24MEM0;
				SD24CCTL0 &= ~SD24LSBACC;                                		// ����� ���� ������ ������� ��� ��� � ��������� ���������� ����� ������ ������� ���� 
				u24_adc = u24_adc_H | u24_adc_L;                                // ���������� ������� � ������� ����� � ��� 0
				*Tx_skolz_okno = u24_adc;                                      	// �����. ����. � ���������� ���� 
				Tx_skolz_okno += 1;
				if (Tx_skolz_okno > (Skolzyashie_okno + N_integr -1))           // ���� �������� � ���������� ���� N_integr ��������
				{
					Tx_skolz_okno = (unsigned long *)& Skolzyashie_okno;        // ��������� ������ ����������� ���� - �� ������ ������
				}
				flag_MEM = 0;                                            		// ���� ����� ������� ��� ���
			} 
			temp_adc = SD24MEM1;												// �����������
        	break;
        } 
        case SD24IV_SD24MEM2: break;
        case SD24IV_SD24MEM3: break;
        default: break;
    }
}


//==========================================================================================
// �/� ������ � ���
//==========================================================================================
void DAC_send_data (unsigned char DAC_Addr, unsigned long DAC_data)
{
	long SPI_data = 0x00000000;                                                 // ���������� ���������� ��� �������� �� SPI � ���
	SPI_data += DAC_Addr;                                                       // ��������� � ����������-����������� 8 ��� ������ ��� �������� �� SPI
	SPI_data = SPI_data << 16;                                                  // �������� ������ �� 16 ��������, ��� ������
	SPI_data += DAC_data;                                                       // ��������� � ����������-����������� 16 ��� ������ ��� �������� �� SPI
	PORT_DAC &= ~(1<<CSB_DAC);                                                  // ������������� � 0 ����� CSB
	_NOP(); //__delay_cyles(1680);
	for (unsigned char dac_i = 0; dac_i < 24; dac_i++)
	{
		if (SPI_data  & 0x800000)                                               // ������� ��� ��������� � 24 ���� ������������ ������, ���� 1, ��
		{
			PORT_DAC |= (1<<SDI_DAC);                                           // ������ ���. 1 �� SDI ���
		}
		else
		{
			PORT_DAC &= ~(1<<SDI_DAC);                                          // ������ ���. 0 �� SDI ���
		}
		PORT_DAC |= (1<<SCLK_DAC);                                              // ������������ CLK ��� �������� �����
		_NOP(); //__delay_cyles(1680);
		PORT_DAC &= ~(1<<SDI_DAC);                                              // ������ ���. 0 �� SDI ��� 
		_NOP(); //__delay_cyles(1680);
		PORT_DAC &= ~(1<<SCLK_DAC);                                             // ������������ CLK ��� ������ �����
		_NOP(); //__delay_cyles(1680);
		SPI_data = SPI_data << 1;                                               // �������� ������ ����� �� 1 ������
	}
	_NOP(); 
	PORT_DAC |= (1<<CSB_DAC);                                                   // ������������� � 1 ����� CSB
	PORT_DAC &= ~(1<<SDI_DAC);                                                  // ������ ���. 0 �� SDI ���
}


//==========================================================================================
//  �/� ���������� ���  
//==========================================================================================
void OUT_current (void)
{
	unsigned long DAC_code_current = 0;                                         // ���������� ��� �������� ���� ��� ��� �������� ����
	// ��� ������ ���� ����� ��� �������������� �� ������, � MCU ���
	DAC_send_data(0x05, 0x1403);                                                // ��������� ��� - ���������� � �������� ERR_CONFIG ������ �� �������� SPI � ������ ������� ����� 

	if (loop_current < 3.50) loop_current = 3.50;                               // ������ �������� � ��������� ��������� �������� ����
	if (loop_current > 23.0) loop_current = 23.0;                               //

	DAC_code_current = b_current + (long)((loop_current - 4.0)/k_current);      // �������� ��� ��� �������� � ���
	DAC_send_data(0x04, DAC_code_current);                                      // ���������� ��� � ���
}






//==========================================================================================
// �/� ��������� ������ ��� ������ ����������
//==========================================================================================
void Led_error_ind (void)
{
	if((zero_but_process_flag == 0) && (zero_mag_process_flag == 0))                        // ���� �� �������� ��������� �������
	{
		if(type_error == ALL_OK) _NOP()/*LED_ON*/;                                                    // ���� ������ ���

		if(tim_err_flag == 0xFF)                                                            // ���� ������ �������� ����������� ��������� ��������
		{
			err_itter += 1;                                                                 // ������� �� ��������� ���������
			if(err_itter > (2*type_error + 5))                                              // ���� ��������� ������ ���������, �� �������� �������
			{
				err_itter = 0;
				if(type_error == SET_ZERO_ERR)                                              // ���� ��������� ���������� 3-�� ������, ��� ��������� � ������� ��� ������
				{
					if(count_error_ind == 1)												// ��������� ������� ��� �������� ������ ������, ���� 2 ����, ��
					{
						flag_error = 0;                                                     // ��������� ��������� ������ � �������
						Set_error(ALL_OK);                                                  // ���������� ������ ������
						count_error_ind = 0;												// ����� �������� ���-�� ��������� ����� � ��� �� ������
						if(zero_but_process_itteration == 3)								// ���� 3-� ������ ������������ ������� ��������� � ������, ��
						{
							zero_but_process_flag = 1;										// ����� ������������ � ������������ ��������� � ������
							zero_process_count = 0;                                         // �������� ������� �������
							zero_but_process_itteration = 5;								// ������ �� 5-� ��������� ��������� ������ (��������� ������ �� 5 ������)
						}
						if(zero_mag_process_itteration == 1)
						{
							zero_mag_process_flag = 1;										// ����� ������������ � ������������ ��������� � �������
							zero_process_count = 0;                                         // �������� ������� �������
							zero_mag_process_itteration = 3;								// ������ �� 5-� ��������� ��������� ������ (��������� ������ �� 5 ������)
						}	
					}
					else count_error_ind +=1;												// ���� ������ ������ �������� ����� 2 ���, �� +1 � �������� � ����� ���������� ��� ���
				}
			}
			delay_from_err_mass = delay_err_mass[err_itter];                                // �������� �������� ��������� �������� �� ������� � ������� ������� ��������� ���������/ �� ���������
			if(led_state_mass[err_itter]) _NOP()/*LED_ON*/;                                           // �������� �� ������� ������ ���������� (���������/�� ���������)
			else _NOP()/*LED_OFF*/;
			timA0count = 0;                                                                 // ����� �������� ������� �0
			tim_err_flag = 0;                                                               // ����� ����� - ������������ �������� ���������� ���������
		}
	}
}
	


//==========================================================================================
// �/� ��������� ���� ������ � ������������� ����������, ������� ��������� ������ �����������
//==========================================================================================
void Set_error (unsigned char err)
{
	_NOP()/*LED_OFF*/;                                                                                // ����� ���������
	type_error = err;                                                                       // ���������� �����-��� ������
	err_itter = 0;                                                                          // ����� ������� ���������
	tim_err_flag = 0;                                                                       // ����� ����� ������������ ���������� ��������� 
	timA0count = 0;                                                                         // ����� �������� ������� �0
	delay_from_err_mass = delay_err_mass[0];                                                // ��������� ��������� �������� ���������� ��������� ��� ������� 
}


//==========================================================================================
// ������ 4-� �������� ����� �� EEPROM 
//==========================================================================================
void READ_4BYTE (unsigned char *ptr2, unsigned int addr)
{
	Flash_ptr = (unsigned char *)addr;
	*ptr2 = *Flash_ptr;                                                                     // |
	ptr2 += 1; Flash_ptr += 1;                                                              // |
	*ptr2 = *Flash_ptr;                                                                     // |
	ptr2 += 1; Flash_ptr += 1;                                                              // | ��������� 4 ����� ������ � ���������� ������
	*ptr2 = *Flash_ptr;                                                                     // |
	ptr2 += 1; Flash_ptr += 1;                                                              // |
	*ptr2 = *Flash_ptr;                                                                     // |
}


//==========================================================================================
// ������ 1 ����� �� ��PROM
//==========================================================================================
unsigned char EEPROM_read(unsigned int adress)
{
	Flash_ptr = (unsigned char *)adress;                                                    // �������� ������������ flash 0x8000 - 0xFFFF
	return *Flash_ptr;
}


//==========================================================================================
// ������ ������������� �������������
//==========================================================================================
void CALL_read (void)
{
	volatile unsigned char i_e;                                                             //
	CALL_ptr = (unsigned char *) & ca;                                                      // ����������� ��������� �� ������ ������� � �������������� ��������������
	Flash_ptr = (unsigned char *) Addr_Segm1;                                                   // ��������� ��������� ����� EEPROM, ��� ��������� ������������� ������������ 

	for (i_e=0; i_e<96; i_e++)
	{
		*CALL_ptr++ = *Flash_ptr++;                                                         // ������������ ������������
	}
}


//==========================================================================================
// ������ �������� �������
//==========================================================================================
void read_conf (void)
{
	tok_signal_inv =EEPROM_read (Addr_EEPROM_conf + 1);
	edinica_izmer = EEPROM_read (Addr_EEPROM_conf + 2);
	koren =         EEPROM_read (Addr_EEPROM_conf + 3);
	DEV_ID =        EEPROM_read (Addr_EEPROM_conf + 4);
	PREAMBULE =     EEPROM_read (Addr_EEPROM_conf + 5);
	HART_disable =  EEPROM_read (Addr_EEPROM_conf + 6);
	magnit_key_enable = EEPROM_read (Addr_EEPROM_conf + 7);
	current_alarm = EEPROM_read (Addr_EEPROM_conf + 30);

	CALL_ptr = (unsigned char *) & diapazon_v;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 8);

	CALL_ptr = (unsigned char *) & diapazon_n;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 12);

	CALL_ptr = (unsigned char *) & damping_value;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 16);

	CALL_ptr = (unsigned char *) & delta_P;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 20);

	CALL_ptr = (unsigned char *) & k_current;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 24);

	CALL_ptr = (unsigned char *) & b_current;
	*CALL_ptr = EEPROM_read (Addr_EEPROM_conf + 28);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (Addr_EEPROM_conf + 29);

	CALL_ptr = (unsigned char *) & UIN + 2;                                                 // ������ ��������� ������ �������
	*CALL_ptr = EEPROM_read (0xE800 + 0);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (0xE800 + 1);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (0xE800 + 2);
}


//==========================================================================================
// �/� ������ � EEPROM ���������� ������� �� �������� * ������� 2 * 0xF7FF-0xF400
//==========================================================================================
void EEPROM_write_pressure_cal (void)
{
	// ������ �������� � ��������� ��. ����� ��, ��� ��, ��� ��, ��� ����. ��, ������ ��
	unsigned char j = 0;
	unsigned char tmpbuf[19];																// ��������� �����
    Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// ��������� �� ������ EEPROM (������� 2)
    while (j < 19)                                                          				// ��������� ���������
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }
    // �������� � ��������� �������� ����� ��
    unsigned char tmpbuf1[3];																// ��������� �����
    Flash_ptr = (unsigned char *) (Addr_Segm2 + 28);                                 		// ��������� �� ������ EEPROM (������� 2)
    j = 0;
    while (j < 3)                                                          					// ��������� ���������
    {
        tmpbuf1[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	Flash_ptr = (unsigned char *) (Addr_Segm2 + 19);                                        // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
//	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	// ������ ������ � �������
	CALL_ptr = (unsigned char *) & k_pressure;                                              // ����������� ������������� ������� �������������� �� ��������
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) &  b_pressure;                                             // ����������� ������������� �������� �������������� �� ��������
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	*Flash_ptr = 0x00;                                                                      // ���������� ����� ����������� ��� ������ ������������� ���� ���������. (����������� ��� ��������� � ���� ��� ����� - �� ������������ ������������ �� ���������)
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	// ��������������� ��. ����� ��, ��� ��, ��� ��, ��� ����. ��, ������ ��
	Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// ��������� �� ������ EEPROM (������� 2)	
	for (j=0; j<19; j++)
	{
		*Flash_ptr++ = tmpbuf[j];                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	// ��������������� �������� ����� ��
	Flash_ptr = (unsigned char *) (Addr_Segm2 + 28);                                 		// ��������� �� ������ EEPROM (������� 2)	
	for (j=0; j<3; j++)
	{
		*Flash_ptr++ = tmpbuf1[j];                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������ Final Assembly Number * ������� 5 * 0xEBFF-0xE800
//==========================================================================================
void write_EEPROM_ASSEMBLY_NUMBER (unsigned char *prt)
{
	// ������ �������� � ��������� �������� ����� �������
	unsigned char j = 0;
	unsigned char tmpbuf[3];																// ��������� �����
    Flash_ptr = (unsigned char *) Addr_Segm5;                                 				// ��������� �� ������ EEPROM (������� 5)
    while (j < 3)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog 
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) (Addr_Segm5 + 3);                                         // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
//	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              // ���������� �� ������ �������� ����� ��
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	// ��������������� �������� ����� �������
	Flash_ptr = (unsigned char *) (Addr_Segm5);                                 			// ��������� �� ������ EEPROM (������� 5)	
	for (j=0; j<3; j++)
	{
		*Flash_ptr++ = tmpbuf[j];                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������ � EEPROM ������ �� * ������� 2 * 0xF7FF-0xF400
//==========================================================================================
void write_data_PD (unsigned char *prt)
{
	// ������ �������� � ��������� ������������� ���� �� �������� k_pressure, b_pressure, �����, �������� ����� ��
	unsigned char j = 0;
	unsigned char tmpbuf[12];																// ��������� �����
    Flash_ptr = (unsigned char *) (Addr_Segm2 + 19);                                 		// ��������� �� ������ EEPROM (������� 2)
    while (j < 12)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm2;                                               // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
//	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	*Flash_ptr = *prt;                                                                      // ������� ���������
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash
	prt += 4;   
	Flash_ptr += 1;

	for (i_e=0; i_e<4; i_e++)
	{
		*Flash_ptr++ = *prt--;                                                              // ���������� �� ������ ������ ������� ��������
	}
	prt += 8;
	for (i_e=0; i_e<4; i_e++)
	{
		*Flash_ptr++ = *prt--;                                                              // ���������� �� ������ ������ ������ ��������
	}
	prt += 8;
	for (i_e=0; i_e<4; i_e++)
	{
		*Flash_ptr++ = *prt--;                                                              // ���������� �� ������ ������ ����������� ����������
	}

	unsigned char *ptrModel;																// ��������� ���������� ��������� ��� ������ ���� � ������ ��
	ptrModel = (unsigned char *) & modelPD;													// ��������� �� ������ ������� ���� 02550�
	for (i_e=0; i_e<6; i_e++)
	{
		*Flash_ptr++ = *ptrModel++;
		while(!(FCTL3 & WAIT));
	}

	// ��������������� ������������� ���� �� �������� k_pressure, b_pressure,�����, �������� ����� ��
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<12; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������ � EEPROM ����,����,��������� * ������� 4 * 0xEFFF-0xEC00
//==========================================================================================
void write_EEPROM_TEG (unsigned char *prt)
{
	// ������ �������� � ��������� ���������
	unsigned char j = 0;
	unsigned char tmpbuf[24];																// ��������� �����
    Flash_ptr = (unsigned char *) (Addr_Segm4 + 21);                                 		// ��������� �� ������ EEPROM (������� 4)
    while (j < 24)
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm4;                                               // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
//	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������
	
	// ���������� �� ������ ���, ���������, ����
	for (i_e=0; i_e<21; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	// ��������������� ������� ���������
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<24; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������ � EEPROM ��������� * ������� 4 * 0xEFFF-0xEC00
//==========================================================================================
void write_EEPROM_MESSAGE (unsigned char *prt)
{
	// ������ �������� � ��������� ���, ����, ����������
	unsigned char j = 0;
	unsigned char tmpbuf[21];																// ��������� �����
    Flash_ptr = (unsigned char *) Addr_Segm4;                           	      			// ��������� �� ������ EEPROM (������� 4)
    while (j < 21)                                                          				// ��������� ���������
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) (Addr_Segm4 + 21);                                        // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
//	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	// ���������� �� ������ MESSAGE
	for (i_e=0; i_e<24; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	// ��������������� ���, ����, ����������
	Flash_ptr = (unsigned char *) Addr_Segm4;                                        		// ��������� �� ������ �������� ������
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<21; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������ � EEPROM ��������� ������ �� * ������� 2 * 0xF7FF-0xF400
//==========================================================================================
void write_EEPROM_SERIAL_PD (unsigned char *prt)
{
	// ������ �������� � ��������� ������� � ������ �������, ����������� �������� �������, ��. ���������, ������������� ������������ �� �������� k_pressure, b_pressure
	unsigned char j = 0;
	unsigned char tmpbuf[28];																// ��������� �����
    Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// ��������� �� ������ EEPROM (������� 4)
    while (j < 28)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) (Addr_Segm2 + 28);                                        // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
	//FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              // ���������� �� ������ �������� ����� ��
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	// ��������������� ��� ������� �� ���������
	Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// ��������� �� ������ EEPROM (������� 4)
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<28; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();                                
}


//==========================================================================================
// �/� ������ � EEPROM ���������� ������ ��* ������� 5 * 0xFBFF-0xE800
//==========================================================================================
void write_EEPROM_ZAVOD_NUMBER_DD (unsigned char *prt)
{
	// ������ �������� � ��������� Final Assembly Number
	unsigned char j = 0;
	unsigned char tmpbuf[3];																// ��������� �����
    Flash_ptr = (unsigned char *) (Addr_Segm5 + 3);                                 		// ��������� �� ������ EEPROM (������� 5)
    while (j < 3)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm5;                                               // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                       						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
	//FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              // ���������� �� ������ ��������� ����� ��
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	// ��������������� ��� ������� �� ���������
	Flash_ptr = (unsigned char *) (Addr_Segm5 + 3);                                 		// ��������� �� ������ EEPROM (������� 5)
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������ � EEPROM ������������� ������������� * ������� 1 * 0xFBFF-0xF800
//==========================================================================================
void EEPROM_write_call (void)
{
	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm1;                                               // ��������� �� ������ �������� ������
	CALL_ptr  = (unsigned char *) & ca;                                                     //
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                       						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
//	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	for (i_e=0; i_e<96; i_e++)
	{
		*Flash_ptr++ = *CALL_ptr++;                                                         // ���������� �� ������ ���� ������ ������������� �������������
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������ � EEPROM ������������ ������� * ������� 3 * 0xF3FF-0xF000
//==========================================================================================
void EEPROM_write_conf (void)
{
	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	Flash_ptr = (unsigned char *) Addr_EEPROM_conf;                                         // ��������� �� ������ �������� ������
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                       						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
	//FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	// ������ ������ � �������
	*Flash_ptr = N_integr;                                                                  // 32 ��������� ��� ��������������
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = tok_signal_inv;                                                            // �������� ������� ������ 4-20 �� ��� 20-4 ��
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = edinica_izmer;                                                             // ������� ���������
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = koren;                                                                     // ���������������� ����������� ���������
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = DEV_ID;                                                                    // ����� ����������
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = PREAMBULE;                                                                 // ���������� ��������
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = HART_disable;                                                              // ���������� ��������� ������������ �� HART
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = magnit_key_enable;                                                         // ���������� ������ ��������� ������
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	CALL_ptr = (unsigned char *) & diapazon_v;                                              // ������� ������ 250 ���
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & diapazon_n;                                              // ������ ������ 0 ���
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & damping_value;                                           // ����� �������������
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & delta_P;                                                 // �������� ��� ��������� 0 ��������
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & k_current;                                               // �������� ������������ �������� ���
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & b_current;                                               // �������� ������������ �������� ���� ���
	n = 2;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 30;
	*Flash_ptr = current_alarm;                                                             // ��������� �������� ���� ��� ��������� ������ (Alarm)
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 125;
	*Flash_ptr = 0x00;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash
	Flash_ptr += 1;                                                                         //

	CALL_ptr = (unsigned char *) Addr_EEPROM_conf;

	CRC16 (CALL_ptr, 126);                                                                  // ����������� �������� CRC ��� ����� ������ � ����������� �������

	*Flash_ptr = crc_H;                                                                     //
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = crc_L;                                                                     //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}


//==========================================================================================
// �/� ������� CRC16 �������� �������� 0xA001 
//==========================================================================================
void CRC16 (unsigned char *nach, unsigned char dlina)
{
	unsigned char j,flag_CRC;
	unsigned int temp_CRC;
	temp_CRC = 0xFFFF;
	while(dlina--)
	{
		temp_CRC ^= *nach++;
		for(j=0; j<8; j++)
		{
			flag_CRC = (unsigned char)(temp_CRC & 0x0001);
			temp_CRC = temp_CRC >> 1;
			if (flag_CRC)
			temp_CRC ^= 0xA001;
		}
	}
	crc_H= (unsigned char)(temp_CRC & 0x00FF);
	crc_L= (unsigned char)((temp_CRC & 0xFF00)>>8);
}


//==========================================================================================
// �/� ������ �������� ����������� ������������ ������������� ������� �� EEPROM
//==========================================================================================
void read_store_conf (void)
{
	tok_signal_inv =EEPROM_read (Addr_cur_EEPROM_conf + 1);
	edinica_izmer = EEPROM_read (Addr_cur_EEPROM_conf + 2);
	koren =         EEPROM_read (Addr_cur_EEPROM_conf + 3);
	DEV_ID =        EEPROM_read (Addr_cur_EEPROM_conf + 4);
	PREAMBULE =     EEPROM_read (Addr_cur_EEPROM_conf + 5);
	HART_disable =  EEPROM_read (Addr_cur_EEPROM_conf + 6);
	magnit_key_enable = EEPROM_read (Addr_cur_EEPROM_conf + 7);
	current_alarm = EEPROM_read (Addr_cur_EEPROM_conf + 30);
	//flagLang = EEPROM_read (Addr_cur_EEPROM_conf + 31);

	CALL_ptr = (unsigned char *) & diapazon_v;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 8);

	CALL_ptr = (unsigned char *) & diapazon_n;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 12);

	CALL_ptr = (unsigned char *) & damping_value;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 16);

	CALL_ptr = (unsigned char *) & delta_P;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 20);

	CALL_ptr = (unsigned char *) & k_current;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 24);

	CALL_ptr = (unsigned char *) & b_current;
	*CALL_ptr = EEPROM_read (Addr_cur_EEPROM_conf + 28);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (Addr_cur_EEPROM_conf + 29);
}   


//==========================================================================================
// �/� ������ � EEPROM ������� ������������ ������� * ������� 6 * 0xE7FF-0xE400
//==========================================================================================
void write_current_conf(void)
{
	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf;                                     // ��������� �� ������ �������� ������
    if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                    						// If Info Seg is stil lockedClear LOCKSEG bit

	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
//	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������

	// ������ ������ � �������
	*Flash_ptr = N_integr;                                                                  // 32 ��������� ��� ��������������
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = tok_signal_inv;                                                            // �������� ������� ������ 4-20 �� ��� 20-4 ��
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = edinica_izmer;                                                             // ������� ���������
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = koren;                                                                     // ���������������� ����������� ���������
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = DEV_ID;                                                                    // ����� ����������
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = PREAMBULE;                                                                 // ���������� ��������
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = HART_disable;                                                              // ���������� ��������� ������������ �� HART
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = magnit_key_enable;                                                         // ���������� ������ ��������� ������
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	CALL_ptr = (unsigned char *) & diapazon_v;                                              // ������� ������ 250 ���
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & diapazon_n;                                              // ������ ������ 0 ���
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & damping_value;                                           // ����� �������������
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & delta_P;                                                 // �������� ��� ��������� 0 ��������
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & k_current;                                               // �������� ������������ �������� ���
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & b_current;                                               // �������� ������������ �������� ���� ���
	n = 2;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
		n -= 1;
	}

	Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 30;
	*Flash_ptr = current_alarm;                                                             // ��������� �������� ���� ��� ��������� ������ (Alarm)
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	// Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 31;
	// *Flash_ptr = flagLang;                                                             		// ��������� �������� ����� ����
	// while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 125;
	*Flash_ptr = 0x00;
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash
	Flash_ptr += 1;                                                                         //

	CALL_ptr = (unsigned char *) Addr_cur_EEPROM_conf;

	CRC16 (CALL_ptr, 126);                                                                  // ����������� �������� CRC ��� ����� ������ � ����������� �������

	*Flash_ptr = crc_H;                                                                     //
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	*Flash_ptr = crc_L;                                                                     //
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash

	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
}                       


//==========================================================================================
// �/� ���������� ����� ���� float 
//==========================================================================================
float okruglenie (float abc, unsigned char num)
{
	volatile float znc;
	volatile long y;
	abc = abc * 10 * num + 0.5;
	y = (long) abc;
	znc = (float) y / (10 * num);
	return znc;
}


//==========================================================================================
// �/� �������� � ������� ��. ���������
//==========================================================================================
float perevod_v_davlenie (float znach)
{
	switch (edinica_izmer)
	{
		case 0x0C: {znach = znach * mastb;                break;}                           // ���
		case 0xED: {znach = znach * mastb * 0.00100000;   break;}                           // ���
		case 0x05: {znach = znach * mastb * 7.50060000;   break;}                           // ��. ��. ��.
		case 0xEF: {znach = znach * mastb * 101.972000;   break;}                           // ��. ���. ��.
		case 0x0A: {znach = znach * mastb * 0.01019720;   break;}                           // ���/��2
		case 0xB0: {znach = znach * mastb * 101.972000;   break;}                           // ���/�2
		case 0x07: {znach = znach * mastb * 0.01000000;   break;}                           // ���
		case 0x0B: {znach = znach * mastb * 1000;         break;}                           // ��
		default: break;
	}
	return znach;
}


//==========================================================================================
// �/� �������� � ������� ��. ���������
//==========================================================================================
float perevod_v_uslovnie_edinici (float znach, char edinica)
{
	switch (edinica)
	{
		case 0x0C: {znach = znach / (mastb * 1.);           break;}                         // ���
		case 0xED: {znach = znach / (mastb * 0.00100000);   break;}                         // ���
		case 0x05: {znach = znach / (mastb * 7.50060000);   break;}                         // ��. ��. ��.
		case 0xEF: {znach = znach / (mastb * 101.972000);   break;}                         // ��. ���. ��.
		case 0x0A: {znach = znach / (mastb * 0.01019720);   break;}                         // ���/��2
		case 0xB0: {znach = znach / (mastb * 101.972000);   break;}                         // ���/�2
		case 0x07: {znach = znach / (mastb * 0.01000000);   break;}                         // ���
		case 0x0B: {znach = znach / (mastb * 1000);         break;}                         // ��
		default: break;
	}
	return znach;
}


//==========================================================================================
// �/� ��� �������� ������������ ��������� ����
//==========================================================================================
char proverka_delta_P (float delt_P)
{
	if (delt_P <= 0.05)                                                                     //
	{
		if (delt_P >= -0.05)                                                                //
		{
			return 0;                                                                       // ���������� 0 ���� ������� �������� ���� �� ��������� +-5% �� ���������
		}
		else
		{
			return 2;                                                                       // ���������� 2 ���� �������� ������ -5%
		}
	}
	else return 1;                                                                          // ���������� 1 ���� �������� ������ 5%
}

//==========================================================================================
// �/� ����������� ������ � ������� 
//==========================================================================================
void Search_problem (void)                                                                  
{
	if (type_error == EEPROM_ERR)                                                       // ��� ������ EEPROM ������������ ��������� �/� ������ ���������� ����
	{
		critical_error ();                                                              // ��� ���� ����� ��� ����� �������� � ���� ��� �� �������� �������� ���������� ���� (Hi / Lo)
	}
	else
	{
		if ((p <= diapazon_v * 1.1) && (p >= (diapazon_n - 0.1) * diapazon_v))                  // �������� �� ���������� ��������� �������� +-10% �� �������������� ���������, �� ���� ���� ��� ������ � ����������, ��
		{
			if(flag_error == 1)                                                                 // ���� �� ��� ��� ��������� ���� ������
			{
				if (type_error == PRES_OVER_HI || type_error == PRES_OVER_LO || type_error == SENS_ERR)                      // ���� ���� ������ ���������� �������� ��� ������ ������� -
				{
					if (DEV_ID == 0)                                                            // � ���������� ������� ������� ���� ������ ����� ������� �����.
					{
						flag_i_out_auto = 1;
					}
					flag_error = 0;                                                             // ������� ���� ������� ������ � ���������� �������� ������
					if(type_error != ALL_OK)                                                    // ��� ���� ���� ������ ��� �� ���� ��������, �� ����������, ���� ��� ��������, �� ����������
					{                                                                           // ������� ������ ��� ����, ����� �� ���� ���������� �������� ���������� (��� ���������� Set_error ��������� ������ �� ���������)    
						type_error = ALL_OK;
						Set_error(ALL_OK);                                                     	// ����� ���� ������
					}
				}
			}
		}
		else                                                                                    // �������� ������� �� ���������� �������
		{
			if ((type_error != EEPROM_ERR) && (type_error != PRES_OVER_HI) && (type_error != PRES_OVER_LO) && (type_error != SENS_ERR)) // ���� �� ���������� ����� ���� ������ ��������� � ���������� � �������� + ������ EEPROM ������� �������
			{
				flag_error = 1;                                                                 // ���������� ����, ��� ���� ����� �� ������ � ���������� � ���� ����������� � ����� ������� � ���� ��� ����� ���
				if (p >= diapazon_v * 1.1)    
				{
					if(type_error != PRES_OVER_HI)                                          	// ���� ��� ������ ���� ��� ��� �� ����������, �� �������������
					{
						type_error = PRES_OVER_HI; 
						Set_error(PRES_OVER_HI);                                            	// ��� ������ - ���� ���
					}
				} 
				else                            
				{
					if(type_error != PRES_OVER_LO)                                          	// ���� ��� ������ ���� ��� �� ����������, �� �������������
					{
						type_error = PRES_OVER_LO; 
						Set_error(PRES_OVER_LO);                                            	// ��� ������ - ���� ���
					}	  
				}
			}
			if (type_error != EEPROM_ERR)							            // ���� ��� ������ ������ ��������� ����������������� ������������
			{
				if (b_ADC_mosta < 0x0020 || b_ADC_mosta > 0xFFFFE0)             // ���� ��� ��� ����� ������� �������� - ������������� �������
				{
					if(type_error != SENS_ERR)                                  // ���� ��� ������ - ������������� ������� ��� �� ���������, �� ����������
					{
						type_error = SENS_ERR;                                  // ��� ������ - ������������� �������
						Set_error(SENS_ERR);
						critical_error ();
					}
				}
				else
				{
					if(type_error == SENS_ERR)                                  // ���� ��� ��� �� ������� �� �������, �� ����� ������ ������ �������
					{
						type_error = ALL_OK;                                              		
						Set_error(ALL_OK);
                        if (DEV_ID == 0) flag_i_out_auto = 1;                   // ���� ������ ����� ������� �����.// ����� �� ������ ���� ����                                                        
					}
				}
			} 
		}
	}
}

//==========================================================================================
// �/� ������ � ������ ���������� ������������� ������� 2
//==========================================================================================
void write_EEPROM_diapazon_change (void)
{
	/*
	WDTCTL = WDTPW + WDTHOLD;                                                               // ��������� watchdog
	_DINT();                                                                                // ������ ���� ����������
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) 0xFA00;                                                   // ��������� �� ������ �������� ������
	CALL_ptr = (unsigned char *) & pr;                                                      // �������� ����� ������ �������
	FCTL1 = FWKEY + ERASE;                                                                  // ������������� ��� �������� ��������
	FCTL3 = FWKEY;                                                                          // ������� ��� ������ �� ������ Lock bit
	*Flash_ptr = 0x00;                                                                      // ��� �������� �������� ��������� ��������� ������
	while(FCTL3 & BUSY);                                                                    // �������� ����������.
	FCTL1 = FWKEY + WRT;                                                                    // ������������� ����� ������ ����� ������
													
	for (i_e=0; i_e<64; i_e++)                                                  
	{                                                 
		*Flash_ptr++ = *CALL_ptr++;                                                         // ���������� �� ������ �������� ������������ �� ������� �������-������, �������-������,....
		while(!(FCTL3 & WAIT));                                                             // ������� ���������� ������ �� Flash
	}                                                 
													
	ed_izmer_eeprom = 0x0C;                                                                 // ������� ��������� ��� ������������ ���������
	Flash_ptr = (unsigned char *) 0xFA40;                                                   // ��������� ����� ��� ������ ������ ��������� ��� �������������
	*Flash_ptr = ed_izmer_eeprom;                                                           // ���������� ������� ��������� � ������ �����
	while(!(FCTL3 & WAIT));                                                                 // ������� ���������� ������ �� Flash
													
													
	FCTL1 = FWKEY;                                                                          // ���������� ��� ������ WRT
	while(FCTL3 & BUSY);                                                                    // ���� ��������� flash �������� ������ - ������������� ��� ������ �� ������
	FCTL3 = FWKEY + LOCK;                                                                   // � ��������� ������ ��������
	WDTCTL = WDT_START;                                                                     // �������� watchdog � �������� ��� ������
	_EINT();
	*/
}


//==========================================================================================
// �/� ������ ������ ������ �� ��������� ��� ��������� ��������� � ��������� ���������� ������� � ������� �����
//==========================================================================================
void critical_error (void)
{
	if (DEV_ID == 0)
	{
		flag_i_out_auto = 0;                                                                // ������ � ����� �������������� ����

		if (current_alarm)  loop_current = CRIT_ERR_HIGH;         							// � ����������� �� �������� ��������� ����� ��� �������� ��� ����������� ����������� ������
		else                loop_current = CRIT_ERR_LOW;           							//

		OUT_current ();                                                                     // ������������� ��������� �������� ����.
	}
}

/*
//==========================================================================================
// ������������ ���-��������������
//==========================================================================================
void Scan_dip_switch (void)
{
	ed_izmer_eeprom = EEPROM_read(0xFA40);                                                  // ��������� ���� � ��������� ���������
	if(ed_izmer_eeprom == 0x0C)                                                             // ���� � ������ � ��������� ��������� �������� ��� (�� ���� ������ � ����������� ������������ ������� ������), �� ��������� ��������� ������������ ��� ������������ ������� � ��������� ������ ������������� �� ������
	{

		dip_switch_value = 0x00;  															// ���������� - ��������� ��� �������������� ���������� 00000000 ��� �������� 0�00 (��� ������������� OFF) ������ ��������� ������� ������������� ���������� �� �����
		if((P3IN & (1<<3)) == 0)    dip_switch_value |= (1<<0);                            	// ���� ������������� ON, �� ���������� � 1 ��������������� ���
		if((P3IN & (1<<6)) == 0)    dip_switch_value |= (1<<1); 
		if((P3IN & (1<<5)) == 0)    dip_switch_value |= (1<<2);


		if (dip_switch_value != 0x00)                                                       // ���� ������� ����� ������������ ����� ��� OFF
		{
			CALL_ptr = (unsigned char *) & tmp_diapazon_v;                                  // ��������� ��  ��������� ���������� �������� �������� ������� (�������2)
			READ_4BYTE (CALL_ptr, (0xFA00 + (0x08*dip_switch_value)));                      

			CALL_ptr = (unsigned char *) & tmp_diapazon_n;                                  // ��������� ��  ��������� ���������� �������� ������� ������� (�������2)
			READ_4BYTE (CALL_ptr, (0xFA04 + (0x08*dip_switch_value)));      

			diapazon_v = perevod_v_uslovnie_edinici(tmp_diapazon_v,ed_izmer_eeprom);        // �������� ������� �������� � �������� �������� �� 0 �� 1
			diapazon_n = perevod_v_uslovnie_edinici(tmp_diapazon_n,ed_izmer_eeprom);        // �������� ������ �������� � �������� �������� �� 0 �� 1
		}
	} 
}


//==========================================================================================
// ���������� ������� ������ ���������
//==========================================================================================
void Zero_Button_Event (void) 
{
	if((zero_but_process_flag == 0) && (zero_process_disable_flag == 0) && (zero_but_mag_prioritet_flag == 0) && (type_error == ALL_OK))                    // ���� �� ��� ��� �� ������� ������� ��������� � ������ � �� ����� �� �������� ������, ��
	{
		if((P3IN & (1<<ZERO_Button)) == 0)                                                  // ���� ������ ��������� ������, ��
		{
			zero_but_process_flag = 1;                                                      // ���������� ����, ��� ������� ������� ��������� ������� � ������
			zero_process_count = 0;                                                         // ��������� ��������-�������
			zero_but_process_itteration = 0;                                                // �������� �������� ��������� � ������ 
			zero_but_mag_prioritet_flag = 1;                                                // ������ ��������� ��������� ������
			LED_ON;                                                                         // �������� ��������� 
			// ��� ��������� ������� ���������� �� 5 ������ ������ ������ ���������, �� ����� ������� ��������� ��������
			// ������ ������ ������ ����������� � �������� 1��, ������� ��������� ������
			// � � ������� 30 ������ ����� �� ������ � ���������� 1 ���, ���� ��������� ������� ��� ����, ����� �������� �� 5 ���, ���� �� ������� ������ 3
		}
	}

	if((zero_but_process_flag == 1) && (zero_but_mag_prioritet_flag == 1))                  // ���� ������� ������� ��������� � ������ ��� ���� � ��� ���������
	{
		switch(zero_but_process_itteration)
		{
			case 0:
			{
				if(zero_process_count < 25)                                                 // ���������, ��� ������ �������� ������� � ������� 5 ���
				{
					if((P3IN & (1<<ZERO_Button)) != 0)                                      // ���� ������ � ������� 5 ��� ���������, ��
					{
					zero_but_process_flag = 1;                                              // ������� ��������� �������
					zero_but_process_itteration = 5;                                        // ������ ����� � ����� �������� ���������
					zero_process_count = 0;                                                 // ��������� ��������-�������
					LED_OFF;                                                                // ����� ���������
					}
					else ;
				}
				else                                                                        // ���� �������� ������ � ������� 5 ���
				{
					if ((P3IN & (1<<ZERO_Button)) == 0)                                     // ���� ������ ��� 5 ���, � ������ ��� ��� ������ ��
					{
						zero_but_process_itteration ++;                                     // ��������� �� ��������� ����
						zero_process_count = 0;                                             // ��������� ��������-�������
					}
				}
				break;
			}


			case 1:                                                                         // �������� ������ ����������� � ���� ���� ������ ��������
			{
				if(zero_process_count < 150)                                                // ���� 15 ������ �� ���������� ������
				{
					if((P3IN & (1<<ZERO_Button)) != 0)                                      // ���� ������ ���������, �� ���� �� ��������� ����
					{
						zero_but_process_itteration ++;
						zero_process_count = 0;                                             // ��������� ��������-�������
					}
				}

				else                                                                        // ���� �� ������ ��������� ������, �� ��� ��������� ��������� �����������
				{
					zero_but_process_itteration = 5;                                        // ��������� � ����� ��������
					zero_process_count = 0; 
				}

				break;
			}


			case 2:                                                                         // ���������� ������ ����������� � ���� ��� � ������� 20 ��� ����� ������ �� ������
			{
				if(zero_process_count < 200)
				{
					if((P3IN & (1<<ZERO_Button)) == 0)                                      // ���� ����� ������ �� ������ �� ��������� �� ��������� ����
					{
						zero_but_process_itteration ++;
						zero_process_count = 0;                                             // ��������� ��������-�������
					}
				}
				else                                                                        // ���� ������ ������ �� ������ ������ ��� �� ��������� ��������� �������
				{
					zero_but_process_itteration = 5;
					zero_process_count = 0; 
				}

				break;
			}


			case 3:                                                                         // ���������, ��� ������ ������� �� ������ ������� ���� � 1 �������
			{
				if(zero_process_count < 5)
				{
					if((P3IN & (1<<ZERO_Button)) != 0)                                      // ���� ��������� ������ �������, �� ��� �����������
					{
						zero_but_process_itteration = 5;
						zero_process_count = 0; 
					}
				}

				else
				{
					if((P3IN & (1<<ZERO_Button)) == 0)                                      // ���� ��� ��� ������, �� ����� �������� �������� ���������
					{
					//===============================================
					//==��� ����� ��� ��� ��������� �������
					//===============================================
						if (proverka_delta_P(delta_P + p) == 0)                             // ������ �������� �������� ����
						{
							delta_P += p;                                                   // ������������ �������� �������� ����
							EEPROM_write_conf ();                                           // ��������� ����� �������� ��������� ����
							LED_OFF;
							zero_but_process_itteration ++;
							zero_process_count = 0;                                         // ��������� ��������-�������
						}
						else                                                                // ���� �������� �������� ���� ������� �� ������ - ������� ��������� �� ������
						{
							LED_OFF;
							type_error = SET_ZERO_ERR;                                      // ���������� ������ ������
							Set_error(SET_ZERO_ERR);
							flag_error = 1;                                                 // ��������� ������ ������
							zero_but_process_flag = 0;                                      // �������� ��������� ������������ ���������, ����� �������� ������ ������
							zero_process_disable_flag = 1;                                  // ����� ��������� ������� �� ������ �� �������� ������, ����� ���� ��� ��������� ������ ����������, ���� ���� ����� �������
							Led_error_ind();                                                // ���� ������ ������ ���������, �� ���� �� ���������� ��������
						}
					}
					//=============================
					//===����� ���� ���������
					//============================= 
				}
				break;
			}


			case 4:                                                                         // ���� �������� ��������� ������ �������, �� ����� � ���� ��� ������������� ���������
			{
				if(zero_process_count >= 16)                                                // � ���� ����� ���� ��� ����� ���� ����� �����
				{                                                                           // �� ���� ���������� ���������� zero_process_count
					LED_OFF;                                                                // ������ ��������� �� �������� 0-4, ����� 6-10 ��������
					zero_but_process_itteration = 5;                                        // 10-12 �� ��������, 12- 16 ��������, ����� 16 ��� �� ��������
					zero_process_count = 0;                                                 // � �� ����� ��� ��������� � 5 ���� 
				}
				if((zero_process_count >= 12) && (zero_process_count < 16))     LED_ON;
				if((zero_process_count >= 10) && (zero_process_count < 12))     LED_OFF;
				if((zero_process_count >= 6) && (zero_process_count < 10))      LED_ON;
				if((zero_process_count >= 2) && (zero_process_count < 4))       LED_OFF;
				break;
			}

			case 5:                                                                         // ���� ����������� � ������ ������������ ������� ������ ���� ����� ���������� �������� ����� ����������� ��������� ������� ����������
			{
				LED_OFF;
				if(zero_process_count >= 25)                                                // ���� ���������� ������� ����� 5 ���
				{
					zero_but_mag_prioritet_flag = 0;                                        // ���� ��������� ������ ��� ���������
					zero_but_process_flag = 0;                                              // ���������� ������� ��������� � ������
					zero_process_count = 0;                                                 // �������� ������� �������
					zero_process_disable_flag = 0;                                          // ��������� ����� ������� ���������
				}

				break;
			}

			default: break;
		}    
	}
}


//==========================================================================================
// ���������� ������� � ���������� ������ ���������
//==========================================================================================
void Zero_Magnit_Event (void)
{
	if (magnit_key_enable)                                                                  // ���� ��������� ������ ��������� ������
	{
		if((zero_mag_process_flag == 0) && (zero_process_disable_flag == 0) && (zero_but_mag_prioritet_flag == 0) && (type_error == ALL_OK)) // ���� �� ��� ��� ��� �� ������� ������� ��������� ��� ������ ������� � ��� ���� ������� ��������� ��� �� ����
		{
			if((P2IN & (1<<ZERO_Magnit)) == 0)                                              // ���� ������ �������� ������, ��
			{
				zero_mag_process_flag = 1;                                                  // ���������� ����, ��� ������� ������� ��������� �������
				zero_process_count = 0;                                                     // ��������� ��������-�������
				zero_mag_process_itteration = 0;                                            // �������� �������� ��������� � ������� 
				zero_but_mag_prioritet_flag = 2;                                            // ���������� ��������� ��� ��������� ���� � ��������� ������
				// ��� ��������� ������� ���������� �� 10 ������ �������� ������ ��� ���������, �� ��� �����
				// ������ ������ ������ ����������� � �������� 1��,
				// ������ ���������, ��������� ��� ���� ������� ��� ���� � ������ ������ ����� �������� �� 5 ���, ���� ������� ������ 3
				// ���� ������ ������ ������ 10 ������, �� ��������� ������
			}
		}

		if((zero_mag_process_flag == 1) && (zero_but_mag_prioritet_flag == 2))              // ���� ������� ������� ��������� � �������
		{
			switch(zero_mag_process_itteration)
			{
				case 0:																		// ����� ��������� � ��������� ���������
				{
					zero_mag_process_itteration ++;
					break;
				}

				case 1:
				{
					if(zero_process_count < 50)                                             // ���������, ��� ������ �������� � ������� 10 ���
					{
						if((P2IN & (1<<ZERO_Magnit)) != 0)                                  // ���� ������ � ������� 10 ��� ���������, ��
						{
							zero_mag_process_flag = 1;                                      // ������� ��������� �������
							zero_mag_process_itteration = 3;                                // ������ � ����� ��������
							zero_process_count = 0;                                         // ��������� ��������-�������
							LED_OFF;
						}
						else ;
					}
					else
					{
						if ((P2IN & (1<<ZERO_Magnit)) != 0)                                 // ���� ������ ��� ����� 10 ���, � ������ ������
						{
							//===============================================
							//==��� ����� ��� ��� ��������� �������
							//===============================================
							if (proverka_delta_P(delta_P + p) == 0)                         // ������ �������� �������� ����
							{
								delta_P += p;                                               // ������������ �������� �������� ����
								EEPROM_write_conf ();                                       // ��������� ����� �������� ��������� ����
								LED_OFF;
								zero_mag_process_itteration ++;
								zero_process_count = 0;                                     // ��������� ��������-�������
							}
							else                                                            // ���� �������� �������� ���� ������� �� ������ - ������� ��������� �� ������
							{
							//	zero_mag_process_itteration = 3;                            // ����� ���������� �� ������� �� ����������� ���������
								LED_OFF;
								type_error = SET_ZERO_ERR;                                  // ���������� ������ ������
								Set_error(SET_ZERO_ERR);
								flag_error = 1;                                             // ��������� ������ ������

								//zero_process_count = 0;                                     // ��������� ������������ ���������
								zero_mag_process_flag = 0;                                  // ���������� ��� ����������
								zero_process_disable_flag = 1;                              // ����� ��������� ���������� ������� ��� ������� �� ������ �� �������� ������, ����� ���� ��� ��������� ������ ����������, ���� ���� ����� �������
								Led_error_ind();                                            // ���� ������ ������ ���������, �� ���� �� ���������� ��������
							}

							//=============================
							//===����� ���� ���������
							//=============================
						}
						if(zero_process_count > 150)                                        // ���������, ��� ������ ����� �� �������
						{
							zero_mag_process_flag = 1;                                      // ������� ��������� �������
							zero_mag_process_itteration = 3;                                // ������ � ����� ��������
							zero_process_count = 0;                                         // ��������� ��������-�������
							LED_OFF;
						}
					}
					break;
				}

				case 2:                                                                     // ���� �������� ��������� ������ �������, �� ����� � ���� ��� ������������� ���������
				{
					if(zero_process_count >= 16)
					{
						LED_OFF;
						zero_mag_process_itteration = 3;
						zero_process_count = 0; 
					}
					if((zero_process_count >= 12) && (zero_process_count < 16))     LED_ON;
					if((zero_process_count >= 10) && (zero_process_count < 12))     LED_OFF;
					if((zero_process_count >= 6) && (zero_process_count < 10))      LED_ON;
					if((zero_process_count >= 2) && (zero_process_count < 4))       LED_OFF;
					break;
				}

				case 3:                                                                     // ���� ��� �� ����� �� ���, �� ����� ��������� �� 5 ���
				{
					LED_OFF;
					if(zero_process_count >= 25)
					{
						zero_but_mag_prioritet_flag = 0;                                    // ���� ��������� ��������� ������ ��� ���������
						zero_mag_process_flag = 0;
						zero_process_count = 0;
						zero_process_disable_flag = 0;                                      // ��������� ����� ������� ���������
					}
					break;
				}

				default: break;
			}    
		} 
	}
}
*/